var mongoose = require('mongoose');

var ContactSchema = new mongoose.Schema({
   name: String,
   email: String,
   message: String
});

mongoose.model('Contact', ContactSchema);
