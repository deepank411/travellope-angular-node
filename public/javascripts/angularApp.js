var app = angular.module('travelApp' , ['ui.router']);

// directives //
app.directive('slider', function () {
   return {
      restrict: 'C',
      link: function (scope, elem) {
         $(elem).slider({});
      }
   }
});
app.directive('collapsible', function () {
   return {
      restrict: 'C',
      link: function (scope, elem) {
         $(elem).collapsible();
      }
   }
});
app.directive('sdnv', function() {
   return {
      restrict: 'C',
      link: function(scope, elem) {
         $(elem).sideNav();
      }
   }
});

// filters //
app.filter('trustAsResourceUrl', ['$sce', function($sce) {
    return function(val) {
        return $sce.trustAsResourceUrl(val);
    };
}]);
