app.config(['$stateProvider', '$urlRouterProvider',
   function($stateProvider, $urlRouterProvider) {
      $stateProvider
         .state('home', {
            url: '/home',
            templateUrl: '/main.html'
         })
         .state('about', {
            url: '/about',
            templateUrl: '/about.html'
         })
         .state('destination', {
            url: '/destination',
            templateUrl: '/destinations.html',
            controller: 'listController',
            resolve: {
               placePromise: ['placeService', function (placeService) {
                  return placeService.getAll();
               }]
            }
         })
         .state('places', {
            url: '/places/{id}',
            templateUrl: '/location.html',
            controller: 'locationController',
            resolve: {
               place: ['$stateParams', 'placeService', function ($stateParams, placeService) {
                  return placeService.get($stateParams.id)
               }]
            }
         })
         .state('contact', {
            url: '/contact',
            templateUrl: '/contact.html',
            controller: 'contactController'
         })
         .state('404', {
            url: '/404',
            templateUrl: '/404.html'
         });
      $urlRouterProvider.otherwise('home');
}]);
