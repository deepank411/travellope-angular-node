var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

var mongoose = require('mongoose');
var Place = mongoose.model('Place');
var Contact = mongoose.model('Contact');

router.get('/places', function(req, res, next) {
  Place.find(function(err, places){
    if (err){
      return next(err);
    }

    res.json(places);
  });
});

router.param('place', function(req, res, next, id) {
  var query = Place.findById(id);

  query.exec(function (err, place){
    if (err) { return next(err); }
    if (!place) { return next(new Error("can't find place")); }

    req.place = place;
    return next();
  });
});

router.get('/places/:place', function(req, res, next) {
  req.place.populate('places', function(err, place) {
    res.json(place);
  });
});

router.post('/contacts', function(req, res, next){
   var contact = new Contact(req.body);
   contact.save(function(err, post){
      if(err){
         return next(err);
      }
      res.json(contact);
   });
});

module.exports = router;
